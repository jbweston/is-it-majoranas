{-# LANGUAGE OverloadedStrings #-}

module CheckAPI (checkSearchTerm, checkArxivRef) where

import Control.Monad.IO.Class (liftIO)
import Data.Monoid ((<>))
import Data.Text.Lazy (Text, pack)
import Control.Lens ((^.), (.~), (&))
import Text.Regex.Posix

import Data.Aeson ((.=), object)
import Network.HTTP.Types (status400)
import qualified Network.Wreq as W

import Web.Scotty


-- API

return400 :: Text -> ActionM a
return400 message = do
    status status400
    json . object $ [ "error" .= message ]
    finish

quote :: String -> Text
quote x = "'" <> pack x <> "'"

checkSearchTerm :: String -> ActionM ()
checkSearchTerm x =
    if elem x validSearchTerms
        then pure ()
        else return400 $ quote x <> " is not a valid search term"
    where validSearchTerms = ["strongai", "majoranas", "quantumsuperiority"]

checkArxivRef :: String -> ActionM ()
checkArxivRef r
    | null matched = notValidRef
    | otherwise = do
        rsp <- liftIO $ W.headWith opts ("https://arxiv.org/abs/" ++ matched)
        case rsp ^. W.responseStatus . W.statusCode of
            200 -> pure ()
            404 -> notValidRef
            _ -> couldNotFetch
    where matched = r =~ arxivRegex :: String
          arxivRegex = "[0-9]{4}\\.[0-9]{4,5}" :: String
          coolStory _ _ = pure ()
          opts = W.defaults & W.redirects .~ 3 & W.checkResponse .~ Just coolStory
          notValidRef = return400 $ quote r <> " is not a valid arXiv reference"
          couldNotFetch = return400 $ "Could not fetch article " <> quote r
