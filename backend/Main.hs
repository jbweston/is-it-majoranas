{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import System.Environment (getArgs)
import System.Random (randomRIO)
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent (threadDelay)
import Data.Maybe (fromMaybe, listToMaybe)

import Web.Scotty
import CheckAPI (checkSearchTerm, checkArxivRef)


app :: ScottyM ()
app =
    get "/:searchTerm/:ref" $ do
      checkSearchTerm =<< param "searchTerm"
      checkArxivRef =<< param "ref"
      liftIO $ sleepRandom (50, 100)
      json False  -- trollface.jpg


main :: IO ()
main = do
    port <- getPort 5000
    scotty port $ app


-- Utilities

sleepRandom :: (Int, Int) -> IO ()
sleepRandom r = randomRIO r >>= threadDelay . (100000 *)

getPort :: Int -> IO Int
getPort p = fromMaybe p <$> fmap read <$> listToMaybe <$> getArgs
