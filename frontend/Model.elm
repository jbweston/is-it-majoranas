module Model exposing (..)

import Http
import Http.Xml
import Xml.Decode as X
import Json.Decode as J

import RemoteData exposing (WebData)


type alias Model =
  { arxivRef : Maybe String
  , arxivRefError : Maybe ArxivRefError
  , arxivRefName : WebData String
  , searchTerm : SearchTerm
  , isScience : WebData Bool
  , err : WebData String
  }

type ArxivRefError
  = NotProvided
  | IsNotArxivRef

type SearchTerm
  = PhysicsBeyondStandardModel
  | KillerApp
  | Majoranas
  | QuantumSuperiority

searchTerms = [Majoranas, QuantumSuperiority, KillerApp, PhysicsBeyondStandardModel]

toLabel : SearchTerm -> String
toLabel s =
  case s of
    PhysicsBeyondStandardModel -> "Physics beyond the standard model"
    Majoranas -> "Majorana zero modes"
    QuantumSuperiority -> "Quantum superiority"
    KillerApp -> "\"Killer app\" for quantum computing"

toURL : SearchTerm -> String
toURL = toString >> String.toLower

initialModel : Model
initialModel =
  { arxivRef = Nothing
  , arxivRefError = Nothing
  , arxivRefName = RemoteData.NotAsked
  , searchTerm = Majoranas
  , err = RemoteData.NotAsked
  , isScience = RemoteData.NotAsked
  }

type Msg
  = GetResult
  | OnResult (WebData Bool)
  | OnArxivName (WebData String)
  | UpdateArxivRef String
  | Select SearchTerm
  | Reset

-- Generic utilities

maybeString : String -> Maybe String
maybeString s = if s == "" then Nothing else Just s

-- App behaviour

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    GetResult -> case model.arxivRef of
      Nothing -> ({model | arxivRefError = Just NotProvided}, Cmd.none)
      Just ref -> ( {model | arxivRefError = Nothing,
                             arxivRefName = RemoteData.Loading,
                             isScience = RemoteData.Loading}
                  , arxivRequest ref)
    OnArxivName r -> case r of
        RemoteData.Success a -> ( {model | arxivRefName = r,
                                           isScience = RemoteData.Loading}
                                 , apiRequest (Maybe.withDefault "" model.arxivRef) model.searchTerm)
        _ -> ( {model | arxivRefError = Just IsNotArxivRef,
                        arxivRefName = RemoteData.NotAsked,
                        err = r,
                        isScience = RemoteData.NotAsked}
             , Cmd.none)
    OnResult r -> ({model | isScience = r}, Cmd.none)
    UpdateArxivRef r -> ({model | arxivRef = maybeString r}, Cmd.none)
    Select s -> ({model | searchTerm = s}, Cmd.none)
    Reset -> (initialModel, Cmd.none)

arxivRequest: String ->  Cmd Msg
arxivRequest ref =
    let
        xmlReq = Http.Xml.request "GET" []
                 (String.join "/" ["https://export.arxiv.org", "api", "query?id_list=" ++ ref])
                 Http.emptyBody
                 decoder
        decoder = X.path ["entry", "title"] (X.single X.string)
   in
      xmlReq
      |> RemoteData.sendRequest
      |> Cmd.map OnArxivName

apiRequest : String -> SearchTerm ->  Cmd Msg
apiRequest ref search =
  Http.get (String.join "/" ["/api", toURL search, ref]) J.bool
      |> RemoteData.sendRequest
      |> Cmd.map OnResult
