'use strict';

require("normalize.css/normalize.css");
require("skeleton.css/skeleton.css");
require("loaders.css/loaders.css");
require("./styles.scss");

var Elm = require('./Main');
var app = Elm.Main.fullscreen();
