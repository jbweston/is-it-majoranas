module Main exposing (..)

import RemoteData
import Html exposing (Html, program)

import Model exposing (Model, Msg, initialModel, update)
import View

-- views

view : Model -> Html Msg
view model =
  case model.isScience of
    RemoteData.NotAsked -> View.ask model
    RemoteData.Loading -> View.ask model
    RemoteData.Success exists -> View.result exists model
    RemoteData.Failure error -> View.failure error

main : Program Never Model Msg
main =
  program
    { init = (initialModel, Cmd.none)
    , view = view
    , update = update
    , subscriptions = \_ -> Sub.none
    }
