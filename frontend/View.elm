module View exposing (ask, result, failure)

import Http
import RemoteData
import Html exposing (Html, button, div, text, b, input)
import Html.Attributes exposing (placeholder, class)
import Html.Events exposing (onClick, onInput)
import Select exposing (fromSelected_)

import Model exposing (..)

empty_div = div [] []
ndiv = div []
sndiv x = div [] [x]
ninput x = input x []
textDiv x = div [class "text"] [text x]

resetButton = button [ onClick Reset ] [text "Try Again"]

ask : Model -> Html Msg
ask model =
  let loading = model.isScience == RemoteData.Loading
      loading_message =
          if model.isScience == RemoteData.Loading
              then case model.arxivRefName of
                RemoteData.Success a ->
                    [ textDiv "Searching "
                    , textDiv ("\"" ++ a ++ "\"")
                    , textDiv "for"
                    , textDiv (toLabel model.searchTerm)
                    ]
                RemoteData.Loading ->
                    [ text "Retrieving "
                    , text (Maybe.withDefault "" model.arxivRef)
                    ]
                _ -> []
          else []
      loader =
        div [class "overlay"]
          [ div [class "loader-container"]
            [
              div [class "loader"]
              [ div [class "ball-triangle-path"]
                    [ empty_div, empty_div, empty_div]
              ]
            , div [class "light-text"] loading_message
            ]
          ]
      renderArxivRefError err =
        case err of
          Nothing -> []
          Just NotProvided -> [text "arxiv reference not provided"]
          Just IsNotArxivRef -> [text "not a valid arXiv reference"]
      arxivInput err =
        ndiv
            [ (sndiv << ninput)
                [ placeholder "1234.5678"
                , onInput UpdateArxivRef
                ]
            , div [class "error"] [b [] (renderArxivRefError err)]
            ]
  in
    ndiv
    [ ndiv (if loading then [loader] else [])
    , div [class "center"]
        [ ndiv
            [ textDiv "Does "
            , arxivInput model.arxivRefError
            , textDiv " contain definitive proof of"
            , fromSelected_ searchTerms Select toString toLabel model.searchTerm
            , textDiv "?"
            ]
        , ndiv [button [ onClick GetResult ] [text "Ask"]]
        ]
    ]

result : Bool -> Model -> Html Msg
result exists model =
  let
    ref = case model.arxivRefName of
      RemoteData.Success r -> r
      _ -> ""
    tell = if exists then "does" else "does not"
  in
    div [class "center"]
    [ ndiv
      [ textDiv "It seems that"
      , textDiv ("\"" ++ ref ++ "\"")
      , div [class "text"]
            [ b [] [text tell]
            , text " contain definitive proof of"
            ]
      , textDiv (toLabel model.searchTerm)
      ]
    , ndiv [resetButton]
    ]

failure : Http.Error -> Html Msg
failure error =
  div [class "center"]
  [ text "There was a network error"
  , ndiv [resetButton]
  ]
